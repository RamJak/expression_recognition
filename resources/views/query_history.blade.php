{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('expression_type', 'Expression_type:') !!}
			{!! Form::text('expression_type') !!}
		</li>
		<li>
			{!! Form::label('query', 'Query:') !!}
			{!! Form::text('query') !!}
		</li>
		<li>
			{!! Form::label('top', 'Top:') !!}
			{!! Form::text('top') !!}
		</li>
		<li>
			{!! Form::label('limit', 'Limit:') !!}
			{!! Form::text('limit') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}