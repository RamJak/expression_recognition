{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('face_id', 'Face_id:') !!}
			{!! Form::text('face_id') !!}
		</li>
		<li>
			{!! Form::label('url', 'Url:') !!}
			{!! Form::text('url') !!}
		</li>
		<li>
			{!! Form::label('width', 'Width:') !!}
			{!! Form::text('width') !!}
		</li>
		<li>
			{!! Form::label('height', 'Height:') !!}
			{!! Form::text('height') !!}
		</li>
		<li>
			{!! Form::label('top', 'Top:') !!}
			{!! Form::text('top') !!}
		</li>
		<li>
			{!! Form::label('left', 'Left:') !!}
			{!! Form::text('left') !!}
		</li>
		<li>
			{!! Form::label('gender', 'Gender:') !!}
			{!! Form::text('gender') !!}
		</li>
		<li>
			{!! Form::label('age', 'Age:') !!}
			{!! Form::text('age') !!}
		</li>
		<li>
			{!! Form::label('pose_pitch', 'Pose_pitch:') !!}
			{!! Form::text('pose_pitch') !!}
		</li>
		<li>
			{!! Form::label('pose_roll', 'Pose_roll:') !!}
			{!! Form::text('pose_roll') !!}
		</li>
		<li>
			{!! Form::label('pose_yaw', 'Pose_yaw:') !!}
			{!! Form::text('pose_yaw') !!}
		</li>
		<li>
			{!! Form::label('fh_moustache', 'Fh_moustache:') !!}
			{!! Form::text('fh_moustache') !!}
		</li>
		<li>
			{!! Form::label('fh_beard', 'Fh_beard:') !!}
			{!! Form::text('fh_beard') !!}
		</li>
		<li>
			{!! Form::label('fh_sideburns', 'Fh_sideburns:') !!}
			{!! Form::text('fh_sideburns') !!}
		</li>
		<li>
			{!! Form::label('landmark_id', 'Landmark_id:') !!}
			{!! Form::text('landmark_id') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}