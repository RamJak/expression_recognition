@extends('layout')
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css"
          rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> CleanLandmarks / Edit #{{$clean_landmark->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('clean_landmarks.update', $clean_landmark->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('pupil_left_x')) has-error @endif">
                    <label for="pupil_left_x-field">Pupil_left_x</label>
                    <input type="text" id="pupil_left_x-field" name="pupil_left_x" class="form-control"
                           value="{{ $clean_landmark->pupil_left_x }}"/>
                    @if($errors->has("pupil_left_x"))
                        <span class="help-block">{{ $errors->first("pupil_left_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('pupil_left_y')) has-error @endif">
                    <label for="pupil_left_y-field">Pupil_left_y</label>
                    <input type="text" id="pupil_left_y-field" name="pupil_left_y" class="form-control"
                           value="{{ $clean_landmark->pupil_left_y }}"/>
                    @if($errors->has("pupil_left_y"))
                        <span class="help-block">{{ $errors->first("pupil_left_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('pupil_right_x')) has-error @endif">
                    <label for="pupil_right_x-field">Pupil_right_x</label>
                    <input type="text" id="pupil_right_x-field" name="pupil_right_x" class="form-control"
                           value="{{ $clean_landmark->pupil_right_x }}"/>
                    @if($errors->has("pupil_right_x"))
                        <span class="help-block">{{ $errors->first("pupil_right_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('pupil_right_y')) has-error @endif">
                    <label for="pupil_right_y-field">Pupil_right_y</label>
                    <input type="text" id="pupil_right_y-field" name="pupil_right_y" class="form-control"
                           value="{{ $clean_landmark->pupil_right_y }}"/>
                    @if($errors->has("pupil_right_y"))
                        <span class="help-block">{{ $errors->first("pupil_right_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('nose_tip_x')) has-error @endif">
                    <label for="nose_tip_x-field">Nose_tip_x</label>
                    <input type="text" id="nose_tip_x-field" name="nose_tip_x" class="form-control"
                           value="{{ $clean_landmark->nose_tip_x }}"/>
                    @if($errors->has("nose_tip_x"))
                        <span class="help-block">{{ $errors->first("nose_tip_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('nose_tip_y')) has-error @endif">
                    <label for="nose_tip_y-field">Nose_tip_y</label>
                    <input type="text" id="nose_tip_y-field" name="nose_tip_y" class="form-control"
                           value="{{ $clean_landmark->nose_tip_y }}"/>
                    @if($errors->has("nose_tip_y"))
                        <span class="help-block">{{ $errors->first("nose_tip_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('mouth_left_x')) has-error @endif">
                    <label for="mouth_left_x-field">Mouth_left_x</label>
                    <input type="text" id="mouth_left_x-field" name="mouth_left_x" class="form-control"
                           value="{{ $clean_landmark->mouth_left_x }}"/>
                    @if($errors->has("mouth_left_x"))
                        <span class="help-block">{{ $errors->first("mouth_left_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('mouth_left_y')) has-error @endif">
                    <label for="mouth_left_y-field">Mouth_left_y</label>
                    <input type="text" id="mouth_left_y-field" name="mouth_left_y" class="form-control"
                           value="{{ $clean_landmark->mouth_left_y }}"/>
                    @if($errors->has("mouth_left_y"))
                        <span class="help-block">{{ $errors->first("mouth_left_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('mouth_right_x')) has-error @endif">
                    <label for="mouth_right_x-field">Mouth_right_x</label>
                    <input type="text" id="mouth_right_x-field" name="mouth_right_x" class="form-control"
                           value="{{ $clean_landmark->mouth_right_x }}"/>
                    @if($errors->has("mouth_right_x"))
                        <span class="help-block">{{ $errors->first("mouth_right_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('mouth_right_y')) has-error @endif">
                    <label for="mouth_right_y-field">Mouth_right_y</label>
                    <input type="text" id="mouth_right_y-field" name="mouth_right_y" class="form-control"
                           value="{{ $clean_landmark->mouth_right_y }}"/>
                    @if($errors->has("mouth_right_y"))
                        <span class="help-block">{{ $errors->first("mouth_right_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eyebrow_left_outer_x')) has-error @endif">
                    <label for="eyebrow_left_outer_x-field">Eyebrow_left_outer_x</label>
                    <input type="text" id="eyebrow_left_outer_x-field" name="eyebrow_left_outer_x" class="form-control"
                           value="{{ $clean_landmark->eyebrow_left_outer_x }}"/>
                    @if($errors->has("eyebrow_left_outer_x"))
                        <span class="help-block">{{ $errors->first("eyebrow_left_outer_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eyebrow_left_outer_y')) has-error @endif">
                    <label for="eyebrow_left_outer_y-field">Eyebrow_left_outer_y</label>
                    <input type="text" id="eyebrow_left_outer_y-field" name="eyebrow_left_outer_y" class="form-control"
                           value="{{ $clean_landmark->eyebrow_left_outer_y }}"/>
                    @if($errors->has("eyebrow_left_outer_y"))
                        <span class="help-block">{{ $errors->first("eyebrow_left_outer_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eyebrow_left_inner_x')) has-error @endif">
                    <label for="eyebrow_left_inner_x-field">Eyebrow_left_inner_x</label>
                    <input type="text" id="eyebrow_left_inner_x-field" name="eyebrow_left_inner_x" class="form-control"
                           value="{{ $clean_landmark->eyebrow_left_inner_x }}"/>
                    @if($errors->has("eyebrow_left_inner_x"))
                        <span class="help-block">{{ $errors->first("eyebrow_left_inner_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eyebrow_left_inner_y')) has-error @endif">
                    <label for="eyebrow_left_inner_y-field">Eyebrow_left_inner_y</label>
                    <input type="text" id="eyebrow_left_inner_y-field" name="eyebrow_left_inner_y" class="form-control"
                           value="{{ $clean_landmark->eyebrow_left_inner_y }}"/>
                    @if($errors->has("eyebrow_left_inner_y"))
                        <span class="help-block">{{ $errors->first("eyebrow_left_inner_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eye_left_outer_x')) has-error @endif">
                    <label for="eye_left_outer_x-field">Eye_left_outer_x</label>
                    <input type="text" id="eye_left_outer_x-field" name="eye_left_outer_x" class="form-control"
                           value="{{ $clean_landmark->eye_left_outer_x }}"/>
                    @if($errors->has("eye_left_outer_x"))
                        <span class="help-block">{{ $errors->first("eye_left_outer_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eye_left_outer_y')) has-error @endif">
                    <label for="eye_left_outer_y-field">Eye_left_outer_y</label>
                    <input type="text" id="eye_left_outer_y-field" name="eye_left_outer_y" class="form-control"
                           value="{{ $clean_landmark->eye_left_outer_y }}"/>
                    @if($errors->has("eye_left_outer_y"))
                        <span class="help-block">{{ $errors->first("eye_left_outer_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eye_left_top_x')) has-error @endif">
                    <label for="eye_left_top_x-field">Eye_left_top_x</label>
                    <input type="text" id="eye_left_top_x-field" name="eye_left_top_x" class="form-control"
                           value="{{ $clean_landmark->eye_left_top_x }}"/>
                    @if($errors->has("eye_left_top_x"))
                        <span class="help-block">{{ $errors->first("eye_left_top_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eye_left_top_y')) has-error @endif">
                    <label for="eye_left_top_y-field">Eye_left_top_y</label>
                    <input type="text" id="eye_left_top_y-field" name="eye_left_top_y" class="form-control"
                           value="{{ $clean_landmark->eye_left_top_y }}"/>
                    @if($errors->has("eye_left_top_y"))
                        <span class="help-block">{{ $errors->first("eye_left_top_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eye_left_bottom_x')) has-error @endif">
                    <label for="eye_left_bottom_x-field">Eye_left_bottom_x</label>
                    <input type="text" id="eye_left_bottom_x-field" name="eye_left_bottom_x" class="form-control"
                           value="{{ $clean_landmark->eye_left_bottom_x }}"/>
                    @if($errors->has("eye_left_bottom_x"))
                        <span class="help-block">{{ $errors->first("eye_left_bottom_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eye_left_bottom_y')) has-error @endif">
                    <label for="eye_left_bottom_y-field">Eye_left_bottom_y</label>
                    <input type="text" id="eye_left_bottom_y-field" name="eye_left_bottom_y" class="form-control"
                           value="{{ $clean_landmark->eye_left_bottom_y }}"/>
                    @if($errors->has("eye_left_bottom_y"))
                        <span class="help-block">{{ $errors->first("eye_left_bottom_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eye_left_inner_x')) has-error @endif">
                    <label for="eye_left_inner_x-field">Eye_left_inner_x</label>
                    <input type="text" id="eye_left_inner_x-field" name="eye_left_inner_x" class="form-control"
                           value="{{ $clean_landmark->eye_left_inner_x }}"/>
                    @if($errors->has("eye_left_inner_x"))
                        <span class="help-block">{{ $errors->first("eye_left_inner_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eye_left_inner_y')) has-error @endif">
                    <label for="eye_left_inner_y-field">Eye_left_inner_y</label>
                    <input type="text" id="eye_left_inner_y-field" name="eye_left_inner_y" class="form-control"
                           value="{{ $clean_landmark->eye_left_inner_y }}"/>
                    @if($errors->has("eye_left_inner_y"))
                        <span class="help-block">{{ $errors->first("eye_left_inner_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eyebrow_right_inner_x')) has-error @endif">
                    <label for="eyebrow_right_inner_x-field">Eyebrow_right_inner_x</label>
                    <input type="text" id="eyebrow_right_inner_x-field" name="eyebrow_right_inner_x"
                           class="form-control" value="{{ $clean_landmark->eyebrow_right_inner_x }}"/>
                    @if($errors->has("eyebrow_right_inner_x"))
                        <span class="help-block">{{ $errors->first("eyebrow_right_inner_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eyebrow_right_inner_y')) has-error @endif">
                    <label for="eyebrow_right_inner_y-field">Eyebrow_right_inner_y</label>
                    <input type="text" id="eyebrow_right_inner_y-field" name="eyebrow_right_inner_y"
                           class="form-control" value="{{ $clean_landmark->eyebrow_right_inner_y }}"/>
                    @if($errors->has("eyebrow_right_inner_y"))
                        <span class="help-block">{{ $errors->first("eyebrow_right_inner_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eyebrow_right_outer_x')) has-error @endif">
                    <label for="eyebrow_right_outer_x-field">Eyebrow_right_outer_x</label>
                    <input type="text" id="eyebrow_right_outer_x-field" name="eyebrow_right_outer_x"
                           class="form-control" value="{{ $clean_landmark->eyebrow_right_outer_x }}"/>
                    @if($errors->has("eyebrow_right_outer_x"))
                        <span class="help-block">{{ $errors->first("eyebrow_right_outer_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eyebrow_right_outer_y')) has-error @endif">
                    <label for="eyebrow_right_outer_y-field">Eyebrow_right_outer_y</label>
                    <input type="text" id="eyebrow_right_outer_y-field" name="eyebrow_right_outer_y"
                           class="form-control" value="{{ $clean_landmark->eyebrow_right_outer_y }}"/>
                    @if($errors->has("eyebrow_right_outer_y"))
                        <span class="help-block">{{ $errors->first("eyebrow_right_outer_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eye_right_inner_x')) has-error @endif">
                    <label for="eye_right_inner_x-field">Eye_right_inner_x</label>
                    <input type="text" id="eye_right_inner_x-field" name="eye_right_inner_x" class="form-control"
                           value="{{ $clean_landmark->eye_right_inner_x }}"/>
                    @if($errors->has("eye_right_inner_x"))
                        <span class="help-block">{{ $errors->first("eye_right_inner_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eye_right_inner_y')) has-error @endif">
                    <label for="eye_right_inner_y-field">Eye_right_inner_y</label>
                    <input type="text" id="eye_right_inner_y-field" name="eye_right_inner_y" class="form-control"
                           value="{{ $clean_landmark->eye_right_inner_y }}"/>
                    @if($errors->has("eye_right_inner_y"))
                        <span class="help-block">{{ $errors->first("eye_right_inner_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eye_right_top_x')) has-error @endif">
                    <label for="eye_right_top_x-field">Eye_right_top_x</label>
                    <input type="text" id="eye_right_top_x-field" name="eye_right_top_x" class="form-control"
                           value="{{ $clean_landmark->eye_right_top_x }}"/>
                    @if($errors->has("eye_right_top_x"))
                        <span class="help-block">{{ $errors->first("eye_right_top_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eye_right_top_y')) has-error @endif">
                    <label for="eye_right_top_y-field">Eye_right_top_y</label>
                    <input type="text" id="eye_right_top_y-field" name="eye_right_top_y" class="form-control"
                           value="{{ $clean_landmark->eye_right_top_y }}"/>
                    @if($errors->has("eye_right_top_y"))
                        <span class="help-block">{{ $errors->first("eye_right_top_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eye_right_bottom_x')) has-error @endif">
                    <label for="eye_right_bottom_x-field">Eye_right_bottom_x</label>
                    <input type="text" id="eye_right_bottom_x-field" name="eye_right_bottom_x" class="form-control"
                           value="{{ $clean_landmark->eye_right_bottom_x }}"/>
                    @if($errors->has("eye_right_bottom_x"))
                        <span class="help-block">{{ $errors->first("eye_right_bottom_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eye_right_bottom_y')) has-error @endif">
                    <label for="eye_right_bottom_y-field">Eye_right_bottom_y</label>
                    <input type="text" id="eye_right_bottom_y-field" name="eye_right_bottom_y" class="form-control"
                           value="{{ $clean_landmark->eye_right_bottom_y }}"/>
                    @if($errors->has("eye_right_bottom_y"))
                        <span class="help-block">{{ $errors->first("eye_right_bottom_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eye_right_outer_x')) has-error @endif">
                    <label for="eye_right_outer_x-field">Eye_right_outer_x</label>
                    <input type="text" id="eye_right_outer_x-field" name="eye_right_outer_x" class="form-control"
                           value="{{ $clean_landmark->eye_right_outer_x }}"/>
                    @if($errors->has("eye_right_outer_x"))
                        <span class="help-block">{{ $errors->first("eye_right_outer_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('eye_right_outer_y')) has-error @endif">
                    <label for="eye_right_outer_y-field">Eye_right_outer_y</label>
                    <input type="text" id="eye_right_outer_y-field" name="eye_right_outer_y" class="form-control"
                           value="{{ $clean_landmark->eye_right_outer_y }}"/>
                    @if($errors->has("eye_right_outer_y"))
                        <span class="help-block">{{ $errors->first("eye_right_outer_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('nose_root_left_x')) has-error @endif">
                    <label for="nose_root_left_x-field">Nose_root_left_x</label>
                    <input type="text" id="nose_root_left_x-field" name="nose_root_left_x" class="form-control"
                           value="{{ $clean_landmark->nose_root_left_x }}"/>
                    @if($errors->has("nose_root_left_x"))
                        <span class="help-block">{{ $errors->first("nose_root_left_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('nose_root_left_y')) has-error @endif">
                    <label for="nose_root_left_y-field">Nose_root_left_y</label>
                    <input type="text" id="nose_root_left_y-field" name="nose_root_left_y" class="form-control"
                           value="{{ $clean_landmark->nose_root_left_y }}"/>
                    @if($errors->has("nose_root_left_y"))
                        <span class="help-block">{{ $errors->first("nose_root_left_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('nose_root_right_x')) has-error @endif">
                    <label for="nose_root_right_x-field">Nose_root_right_x</label>
                    <input type="text" id="nose_root_right_x-field" name="nose_root_right_x" class="form-control"
                           value="{{ $clean_landmark->nose_root_right_x }}"/>
                    @if($errors->has("nose_root_right_x"))
                        <span class="help-block">{{ $errors->first("nose_root_right_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('nose_root_right_y')) has-error @endif">
                    <label for="nose_root_right_y-field">Nose_root_right_y</label>
                    <input type="text" id="nose_root_right_y-field" name="nose_root_right_y" class="form-control"
                           value="{{ $clean_landmark->nose_root_right_y }}"/>
                    @if($errors->has("nose_root_right_y"))
                        <span class="help-block">{{ $errors->first("nose_root_right_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('nose_left_alar_top_x')) has-error @endif">
                    <label for="nose_left_alar_top_x-field">Nose_left_alar_top_x</label>
                    <input type="text" id="nose_left_alar_top_x-field" name="nose_left_alar_top_x" class="form-control"
                           value="{{ $clean_landmark->nose_left_alar_top_x }}"/>
                    @if($errors->has("nose_left_alar_top_x"))
                        <span class="help-block">{{ $errors->first("nose_left_alar_top_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('nose_left_alar_top_y')) has-error @endif">
                    <label for="nose_left_alar_top_y-field">Nose_left_alar_top_y</label>
                    <input type="text" id="nose_left_alar_top_y-field" name="nose_left_alar_top_y" class="form-control"
                           value="{{ $clean_landmark->nose_left_alar_top_y }}"/>
                    @if($errors->has("nose_left_alar_top_y"))
                        <span class="help-block">{{ $errors->first("nose_left_alar_top_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('nose_right_alar_top_x')) has-error @endif">
                    <label for="nose_right_alar_top_x-field">Nose_right_alar_top_x</label>
                    <input type="text" id="nose_right_alar_top_x-field" name="nose_right_alar_top_x"
                           class="form-control" value="{{ $clean_landmark->nose_right_alar_top_x }}"/>
                    @if($errors->has("nose_right_alar_top_x"))
                        <span class="help-block">{{ $errors->first("nose_right_alar_top_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('nose_right_alar_top_y')) has-error @endif">
                    <label for="nose_right_alar_top_y-field">Nose_right_alar_top_y</label>
                    <input type="text" id="nose_right_alar_top_y-field" name="nose_right_alar_top_y"
                           class="form-control" value="{{ $clean_landmark->nose_right_alar_top_y }}"/>
                    @if($errors->has("nose_right_alar_top_y"))
                        <span class="help-block">{{ $errors->first("nose_right_alar_top_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('nose_left_alar_out_tip_x')) has-error @endif">
                    <label for="nose_left_alar_out_tip_x-field">Nose_left_alar_out_tip_x</label>
                    <input type="text" id="nose_left_alar_out_tip_x-field" name="nose_left_alar_out_tip_x"
                           class="form-control" value="{{ $clean_landmark->nose_left_alar_out_tip_x }}"/>
                    @if($errors->has("nose_left_alar_out_tip_x"))
                        <span class="help-block">{{ $errors->first("nose_left_alar_out_tip_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('nose_left_alar_out_tip_y')) has-error @endif">
                    <label for="nose_left_alar_out_tip_y-field">Nose_left_alar_out_tip_y</label>
                    <input type="text" id="nose_left_alar_out_tip_y-field" name="nose_left_alar_out_tip_y"
                           class="form-control" value="{{ $clean_landmark->nose_left_alar_out_tip_y }}"/>
                    @if($errors->has("nose_left_alar_out_tip_y"))
                        <span class="help-block">{{ $errors->first("nose_left_alar_out_tip_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('nose_right_alar_out_tip_x')) has-error @endif">
                    <label for="nose_right_alar_out_tip_x-field">Nose_right_alar_out_tip_x</label>
                    <input type="text" id="nose_right_alar_out_tip_x-field" name="nose_right_alar_out_tip_x"
                           class="form-control" value="{{ $clean_landmark->nose_right_alar_out_tip_x }}"/>
                    @if($errors->has("nose_right_alar_out_tip_x"))
                        <span class="help-block">{{ $errors->first("nose_right_alar_out_tip_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('nose_right_alar_out_tip_y')) has-error @endif">
                    <label for="nose_right_alar_out_tip_y-field">Nose_right_alar_out_tip_y</label>
                    <input type="text" id="nose_right_alar_out_tip_y-field" name="nose_right_alar_out_tip_y"
                           class="form-control" value="{{ $clean_landmark->nose_right_alar_out_tip_y }}"/>
                    @if($errors->has("nose_right_alar_out_tip_y"))
                        <span class="help-block">{{ $errors->first("nose_right_alar_out_tip_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('upper_lip_top_x')) has-error @endif">
                    <label for="upper_lip_top_x-field">Upper_lip_top_x</label>
                    <input type="text" id="upper_lip_top_x-field" name="upper_lip_top_x" class="form-control"
                           value="{{ $clean_landmark->upper_lip_top_x }}"/>
                    @if($errors->has("upper_lip_top_x"))
                        <span class="help-block">{{ $errors->first("upper_lip_top_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('upper_lip_top_y')) has-error @endif">
                    <label for="upper_lip_top_y-field">Upper_lip_top_y</label>
                    <input type="text" id="upper_lip_top_y-field" name="upper_lip_top_y" class="form-control"
                           value="{{ $clean_landmark->upper_lip_top_y }}"/>
                    @if($errors->has("upper_lip_top_y"))
                        <span class="help-block">{{ $errors->first("upper_lip_top_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('upper_lip_bottom_x')) has-error @endif">
                    <label for="upper_lip_bottom_x-field">Upper_lip_bottom_x</label>
                    <input type="text" id="upper_lip_bottom_x-field" name="upper_lip_bottom_x" class="form-control"
                           value="{{ $clean_landmark->upper_lip_bottom_x }}"/>
                    @if($errors->has("upper_lip_bottom_x"))
                        <span class="help-block">{{ $errors->first("upper_lip_bottom_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('upper_lip_bottom_y')) has-error @endif">
                    <label for="upper_lip_bottom_y-field">Upper_lip_bottom_y</label>
                    <input type="text" id="upper_lip_bottom_y-field" name="upper_lip_bottom_y" class="form-control"
                           value="{{ $clean_landmark->upper_lip_bottom_y }}"/>
                    @if($errors->has("upper_lip_bottom_y"))
                        <span class="help-block">{{ $errors->first("upper_lip_bottom_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('under_lip_top_x')) has-error @endif">
                    <label for="under_lip_top_x-field">Under_lip_top_x</label>
                    <input type="text" id="under_lip_top_x-field" name="under_lip_top_x" class="form-control"
                           value="{{ $clean_landmark->under_lip_top_x }}"/>
                    @if($errors->has("under_lip_top_x"))
                        <span class="help-block">{{ $errors->first("under_lip_top_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('under_lip_top_y')) has-error @endif">
                    <label for="under_lip_top_y-field">Under_lip_top_y</label>
                    <input type="text" id="under_lip_top_y-field" name="under_lip_top_y" class="form-control"
                           value="{{ $clean_landmark->under_lip_top_y }}"/>
                    @if($errors->has("under_lip_top_y"))
                        <span class="help-block">{{ $errors->first("under_lip_top_y") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('under_lip_bottom_x')) has-error @endif">
                    <label for="under_lip_bottom_x-field">Under_lip_bottom_x</label>
                    <input type="text" id="under_lip_bottom_x-field" name="under_lip_bottom_x" class="form-control"
                           value="{{ $clean_landmark->under_lip_bottom_x }}"/>
                    @if($errors->has("under_lip_bottom_x"))
                        <span class="help-block">{{ $errors->first("under_lip_bottom_x") }}</span>
                    @endif
                </div>
                <div class="form-group @if($errors->has('under_lip_bottom_y')) has-error @endif">
                    <label for="under_lip_bottom_y-field">Under_lip_bottom_y</label>
                    <input type="text" id="under_lip_bottom_y-field" name="under_lip_bottom_y" class="form-control"
                           value="{{ $clean_landmark->under_lip_bottom_y }}"/>
                    @if($errors->has("under_lip_bottom_y"))
                        <span class="help-block">{{ $errors->first("under_lip_bottom_y") }}</span>
                    @endif
                </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('clean_landmarks.index') }}"><i
                                class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        $('.date-picker').datepicker({});
    </script>
@endsection
