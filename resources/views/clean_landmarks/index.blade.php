@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> CleanLandmarks
            <a class="btn btn-success pull-right" href="{{ route('clean_landmarks.create') }}"><i
                        class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($clean_landmarks->count())
                <table class="table table-condensed table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>PUPIL_LEFT_X</th>
                        <th>PUPIL_LEFT_Y</th>
                        <th>PUPIL_RIGHT_X</th>
                        <th>PUPIL_RIGHT_Y</th>
                        <th>NOSE_TIP_X</th>
                        <th>NOSE_TIP_Y</th>
                        <th>MOUTH_LEFT_X</th>
                        <th>MOUTH_LEFT_Y</th>
                        <th>MOUTH_RIGHT_X</th>
                        <th>MOUTH_RIGHT_Y</th>
                        <th>EYEBROW_LEFT_OUTER_X</th>
                        <th>EYEBROW_LEFT_OUTER_Y</th>
                        <th>EYEBROW_LEFT_INNER_X</th>
                        <th>EYEBROW_LEFT_INNER_Y</th>
                        <th>EYE_LEFT_OUTER_X</th>
                        <th>EYE_LEFT_OUTER_Y</th>
                        <th>EYE_LEFT_TOP_X</th>
                        <th>EYE_LEFT_TOP_Y</th>
                        <th>EYE_LEFT_BOTTOM_X</th>
                        <th>EYE_LEFT_BOTTOM_Y</th>
                        <th>EYE_LEFT_INNER_X</th>
                        <th>EYE_LEFT_INNER_Y</th>
                        <th>EYEBROW_RIGHT_INNER_X</th>
                        <th>EYEBROW_RIGHT_INNER_Y</th>
                        <th>EYEBROW_RIGHT_OUTER_X</th>
                        <th>EYEBROW_RIGHT_OUTER_Y</th>
                        <th>EYE_RIGHT_INNER_X</th>
                        <th>EYE_RIGHT_INNER_Y</th>
                        <th>EYE_RIGHT_TOP_X</th>
                        <th>EYE_RIGHT_TOP_Y</th>
                        <th>EYE_RIGHT_BOTTOM_X</th>
                        <th>EYE_RIGHT_BOTTOM_Y</th>
                        <th>EYE_RIGHT_OUTER_X</th>
                        <th>EYE_RIGHT_OUTER_Y</th>
                        <th>NOSE_ROOT_LEFT_X</th>
                        <th>NOSE_ROOT_LEFT_Y</th>
                        <th>NOSE_ROOT_RIGHT_X</th>
                        <th>NOSE_ROOT_RIGHT_Y</th>
                        <th>NOSE_LEFT_ALAR_TOP_X</th>
                        <th>NOSE_LEFT_ALAR_TOP_Y</th>
                        <th>NOSE_RIGHT_ALAR_TOP_X</th>
                        <th>NOSE_RIGHT_ALAR_TOP_Y</th>
                        <th>NOSE_LEFT_ALAR_OUT_TIP_X</th>
                        <th>NOSE_LEFT_ALAR_OUT_TIP_Y</th>
                        <th>NOSE_RIGHT_ALAR_OUT_TIP_X</th>
                        <th>NOSE_RIGHT_ALAR_OUT_TIP_Y</th>
                        <th>UPPER_LIP_TOP_X</th>
                        <th>UPPER_LIP_TOP_Y</th>
                        <th>UPPER_LIP_BOTTOM_X</th>
                        <th>UPPER_LIP_BOTTOM_Y</th>
                        <th>UNDER_LIP_TOP_X</th>
                        <th>UNDER_LIP_TOP_Y</th>
                        <th>UNDER_LIP_BOTTOM_X</th>
                        <th>UNDER_LIP_BOTTOM_Y</th>
                        <th class="text-right">OPTIONS</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($clean_landmarks as $clean_landmark)
                        <tr>
                            <td>{{$clean_landmark->id}}</td>
                            <td>{{$clean_landmark->pupil_left_x}}</td>
                            <td>{{$clean_landmark->pupil_left_y}}</td>
                            <td>{{$clean_landmark->pupil_right_x}}</td>
                            <td>{{$clean_landmark->pupil_right_y}}</td>
                            <td>{{$clean_landmark->nose_tip_x}}</td>
                            <td>{{$clean_landmark->nose_tip_y}}</td>
                            <td>{{$clean_landmark->mouth_left_x}}</td>
                            <td>{{$clean_landmark->mouth_left_y}}</td>
                            <td>{{$clean_landmark->mouth_right_x}}</td>
                            <td>{{$clean_landmark->mouth_right_y}}</td>
                            <td>{{$clean_landmark->eyebrow_left_outer_x}}</td>
                            <td>{{$clean_landmark->eyebrow_left_outer_y}}</td>
                            <td>{{$clean_landmark->eyebrow_left_inner_x}}</td>
                            <td>{{$clean_landmark->eyebrow_left_inner_y}}</td>
                            <td>{{$clean_landmark->eye_left_outer_x}}</td>
                            <td>{{$clean_landmark->eye_left_outer_y}}</td>
                            <td>{{$clean_landmark->eye_left_top_x}}</td>
                            <td>{{$clean_landmark->eye_left_top_y}}</td>
                            <td>{{$clean_landmark->eye_left_bottom_x}}</td>
                            <td>{{$clean_landmark->eye_left_bottom_y}}</td>
                            <td>{{$clean_landmark->eye_left_inner_x}}</td>
                            <td>{{$clean_landmark->eye_left_inner_y}}</td>
                            <td>{{$clean_landmark->eyebrow_right_inner_x}}</td>
                            <td>{{$clean_landmark->eyebrow_right_inner_y}}</td>
                            <td>{{$clean_landmark->eyebrow_right_outer_x}}</td>
                            <td>{{$clean_landmark->eyebrow_right_outer_y}}</td>
                            <td>{{$clean_landmark->eye_right_inner_x}}</td>
                            <td>{{$clean_landmark->eye_right_inner_y}}</td>
                            <td>{{$clean_landmark->eye_right_top_x}}</td>
                            <td>{{$clean_landmark->eye_right_top_y}}</td>
                            <td>{{$clean_landmark->eye_right_bottom_x}}</td>
                            <td>{{$clean_landmark->eye_right_bottom_y}}</td>
                            <td>{{$clean_landmark->eye_right_outer_x}}</td>
                            <td>{{$clean_landmark->eye_right_outer_y}}</td>
                            <td>{{$clean_landmark->nose_root_left_x}}</td>
                            <td>{{$clean_landmark->nose_root_left_y}}</td>
                            <td>{{$clean_landmark->nose_root_right_x}}</td>
                            <td>{{$clean_landmark->nose_root_right_y}}</td>
                            <td>{{$clean_landmark->nose_left_alar_top_x}}</td>
                            <td>{{$clean_landmark->nose_left_alar_top_y}}</td>
                            <td>{{$clean_landmark->nose_right_alar_top_x}}</td>
                            <td>{{$clean_landmark->nose_right_alar_top_y}}</td>
                            <td>{{$clean_landmark->nose_left_alar_out_tip_x}}</td>
                            <td>{{$clean_landmark->nose_left_alar_out_tip_y}}</td>
                            <td>{{$clean_landmark->nose_right_alar_out_tip_x}}</td>
                            <td>{{$clean_landmark->nose_right_alar_out_tip_y}}</td>
                            <td>{{$clean_landmark->upper_lip_top_x}}</td>
                            <td>{{$clean_landmark->upper_lip_top_y}}</td>
                            <td>{{$clean_landmark->upper_lip_bottom_x}}</td>
                            <td>{{$clean_landmark->upper_lip_bottom_y}}</td>
                            <td>{{$clean_landmark->under_lip_top_x}}</td>
                            <td>{{$clean_landmark->under_lip_top_y}}</td>
                            <td>{{$clean_landmark->under_lip_bottom_x}}</td>
                            <td>{{$clean_landmark->under_lip_bottom_y}}</td>
                            <td class="text-right">
                                <a class="btn btn-xs btn-primary"
                                   href="{{ route('clean_landmarks.show', $clean_landmark->id) }}"><i
                                            class="glyphicon glyphicon-eye-open"></i> View</a>
                                <a class="btn btn-xs btn-warning"
                                   href="{{ route('clean_landmarks.edit', $clean_landmark->id) }}"><i
                                            class="glyphicon glyphicon-edit"></i> Edit</a>

                                <form action="{{ route('clean_landmarks.destroy', $clean_landmark->id) }}" method="POST"
                                      style="display: inline;"
                                      onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-xs btn-danger"><i
                                                class="glyphicon glyphicon-trash"></i> Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $clean_landmarks->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection