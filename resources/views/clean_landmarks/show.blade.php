@extends('layout')
@section('header')
    <div class="page-header">
        <h1>CleanLandmarks / Show #{{$clean_landmark->id}}</h1>

        <form action="{{ route('clean_landmarks.destroy', $clean_landmark->id) }}" method="POST"
              style="display: inline;"
              onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group"
                   href="{{ route('clean_landmarks.edit', $clean_landmark->id) }}"><i
                            class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>

                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                    <label for="pupil_left_x">PUPIL_LEFT_X</label>

                    <p class="form-control-static">{{$clean_landmark->pupil_left_x}}</p>
                </div>
                <div class="form-group">
                    <label for="pupil_left_y">PUPIL_LEFT_Y</label>

                    <p class="form-control-static">{{$clean_landmark->pupil_left_y}}</p>
                </div>
                <div class="form-group">
                    <label for="pupil_right_x">PUPIL_RIGHT_X</label>

                    <p class="form-control-static">{{$clean_landmark->pupil_right_x}}</p>
                </div>
                <div class="form-group">
                    <label for="pupil_right_y">PUPIL_RIGHT_Y</label>

                    <p class="form-control-static">{{$clean_landmark->pupil_right_y}}</p>
                </div>
                <div class="form-group">
                    <label for="nose_tip_x">NOSE_TIP_X</label>

                    <p class="form-control-static">{{$clean_landmark->nose_tip_x}}</p>
                </div>
                <div class="form-group">
                    <label for="nose_tip_y">NOSE_TIP_Y</label>

                    <p class="form-control-static">{{$clean_landmark->nose_tip_y}}</p>
                </div>
                <div class="form-group">
                    <label for="mouth_left_x">MOUTH_LEFT_X</label>

                    <p class="form-control-static">{{$clean_landmark->mouth_left_x}}</p>
                </div>
                <div class="form-group">
                    <label for="mouth_left_y">MOUTH_LEFT_Y</label>

                    <p class="form-control-static">{{$clean_landmark->mouth_left_y}}</p>
                </div>
                <div class="form-group">
                    <label for="mouth_right_x">MOUTH_RIGHT_X</label>

                    <p class="form-control-static">{{$clean_landmark->mouth_right_x}}</p>
                </div>
                <div class="form-group">
                    <label for="mouth_right_y">MOUTH_RIGHT_Y</label>

                    <p class="form-control-static">{{$clean_landmark->mouth_right_y}}</p>
                </div>
                <div class="form-group">
                    <label for="eyebrow_left_outer_x">EYEBROW_LEFT_OUTER_X</label>

                    <p class="form-control-static">{{$clean_landmark->eyebrow_left_outer_x}}</p>
                </div>
                <div class="form-group">
                    <label for="eyebrow_left_outer_y">EYEBROW_LEFT_OUTER_Y</label>

                    <p class="form-control-static">{{$clean_landmark->eyebrow_left_outer_y}}</p>
                </div>
                <div class="form-group">
                    <label for="eyebrow_left_inner_x">EYEBROW_LEFT_INNER_X</label>

                    <p class="form-control-static">{{$clean_landmark->eyebrow_left_inner_x}}</p>
                </div>
                <div class="form-group">
                    <label for="eyebrow_left_inner_y">EYEBROW_LEFT_INNER_Y</label>

                    <p class="form-control-static">{{$clean_landmark->eyebrow_left_inner_y}}</p>
                </div>
                <div class="form-group">
                    <label for="eye_left_outer_x">EYE_LEFT_OUTER_X</label>

                    <p class="form-control-static">{{$clean_landmark->eye_left_outer_x}}</p>
                </div>
                <div class="form-group">
                    <label for="eye_left_outer_y">EYE_LEFT_OUTER_Y</label>

                    <p class="form-control-static">{{$clean_landmark->eye_left_outer_y}}</p>
                </div>
                <div class="form-group">
                    <label for="eye_left_top_x">EYE_LEFT_TOP_X</label>

                    <p class="form-control-static">{{$clean_landmark->eye_left_top_x}}</p>
                </div>
                <div class="form-group">
                    <label for="eye_left_top_y">EYE_LEFT_TOP_Y</label>

                    <p class="form-control-static">{{$clean_landmark->eye_left_top_y}}</p>
                </div>
                <div class="form-group">
                    <label for="eye_left_bottom_x">EYE_LEFT_BOTTOM_X</label>

                    <p class="form-control-static">{{$clean_landmark->eye_left_bottom_x}}</p>
                </div>
                <div class="form-group">
                    <label for="eye_left_bottom_y">EYE_LEFT_BOTTOM_Y</label>

                    <p class="form-control-static">{{$clean_landmark->eye_left_bottom_y}}</p>
                </div>
                <div class="form-group">
                    <label for="eye_left_inner_x">EYE_LEFT_INNER_X</label>

                    <p class="form-control-static">{{$clean_landmark->eye_left_inner_x}}</p>
                </div>
                <div class="form-group">
                    <label for="eye_left_inner_y">EYE_LEFT_INNER_Y</label>

                    <p class="form-control-static">{{$clean_landmark->eye_left_inner_y}}</p>
                </div>
                <div class="form-group">
                    <label for="eyebrow_right_inner_x">EYEBROW_RIGHT_INNER_X</label>

                    <p class="form-control-static">{{$clean_landmark->eyebrow_right_inner_x}}</p>
                </div>
                <div class="form-group">
                    <label for="eyebrow_right_inner_y">EYEBROW_RIGHT_INNER_Y</label>

                    <p class="form-control-static">{{$clean_landmark->eyebrow_right_inner_y}}</p>
                </div>
                <div class="form-group">
                    <label for="eyebrow_right_outer_x">EYEBROW_RIGHT_OUTER_X</label>

                    <p class="form-control-static">{{$clean_landmark->eyebrow_right_outer_x}}</p>
                </div>
                <div class="form-group">
                    <label for="eyebrow_right_outer_y">EYEBROW_RIGHT_OUTER_Y</label>

                    <p class="form-control-static">{{$clean_landmark->eyebrow_right_outer_y}}</p>
                </div>
                <div class="form-group">
                    <label for="eye_right_inner_x">EYE_RIGHT_INNER_X</label>

                    <p class="form-control-static">{{$clean_landmark->eye_right_inner_x}}</p>
                </div>
                <div class="form-group">
                    <label for="eye_right_inner_y">EYE_RIGHT_INNER_Y</label>

                    <p class="form-control-static">{{$clean_landmark->eye_right_inner_y}}</p>
                </div>
                <div class="form-group">
                    <label for="eye_right_top_x">EYE_RIGHT_TOP_X</label>

                    <p class="form-control-static">{{$clean_landmark->eye_right_top_x}}</p>
                </div>
                <div class="form-group">
                    <label for="eye_right_top_y">EYE_RIGHT_TOP_Y</label>

                    <p class="form-control-static">{{$clean_landmark->eye_right_top_y}}</p>
                </div>
                <div class="form-group">
                    <label for="eye_right_bottom_x">EYE_RIGHT_BOTTOM_X</label>

                    <p class="form-control-static">{{$clean_landmark->eye_right_bottom_x}}</p>
                </div>
                <div class="form-group">
                    <label for="eye_right_bottom_y">EYE_RIGHT_BOTTOM_Y</label>

                    <p class="form-control-static">{{$clean_landmark->eye_right_bottom_y}}</p>
                </div>
                <div class="form-group">
                    <label for="eye_right_outer_x">EYE_RIGHT_OUTER_X</label>

                    <p class="form-control-static">{{$clean_landmark->eye_right_outer_x}}</p>
                </div>
                <div class="form-group">
                    <label for="eye_right_outer_y">EYE_RIGHT_OUTER_Y</label>

                    <p class="form-control-static">{{$clean_landmark->eye_right_outer_y}}</p>
                </div>
                <div class="form-group">
                    <label for="nose_root_left_x">NOSE_ROOT_LEFT_X</label>

                    <p class="form-control-static">{{$clean_landmark->nose_root_left_x}}</p>
                </div>
                <div class="form-group">
                    <label for="nose_root_left_y">NOSE_ROOT_LEFT_Y</label>

                    <p class="form-control-static">{{$clean_landmark->nose_root_left_y}}</p>
                </div>
                <div class="form-group">
                    <label for="nose_root_right_x">NOSE_ROOT_RIGHT_X</label>

                    <p class="form-control-static">{{$clean_landmark->nose_root_right_x}}</p>
                </div>
                <div class="form-group">
                    <label for="nose_root_right_y">NOSE_ROOT_RIGHT_Y</label>

                    <p class="form-control-static">{{$clean_landmark->nose_root_right_y}}</p>
                </div>
                <div class="form-group">
                    <label for="nose_left_alar_top_x">NOSE_LEFT_ALAR_TOP_X</label>

                    <p class="form-control-static">{{$clean_landmark->nose_left_alar_top_x}}</p>
                </div>
                <div class="form-group">
                    <label for="nose_left_alar_top_y">NOSE_LEFT_ALAR_TOP_Y</label>

                    <p class="form-control-static">{{$clean_landmark->nose_left_alar_top_y}}</p>
                </div>
                <div class="form-group">
                    <label for="nose_right_alar_top_x">NOSE_RIGHT_ALAR_TOP_X</label>

                    <p class="form-control-static">{{$clean_landmark->nose_right_alar_top_x}}</p>
                </div>
                <div class="form-group">
                    <label for="nose_right_alar_top_y">NOSE_RIGHT_ALAR_TOP_Y</label>

                    <p class="form-control-static">{{$clean_landmark->nose_right_alar_top_y}}</p>
                </div>
                <div class="form-group">
                    <label for="nose_left_alar_out_tip_x">NOSE_LEFT_ALAR_OUT_TIP_X</label>

                    <p class="form-control-static">{{$clean_landmark->nose_left_alar_out_tip_x}}</p>
                </div>
                <div class="form-group">
                    <label for="nose_left_alar_out_tip_y">NOSE_LEFT_ALAR_OUT_TIP_Y</label>

                    <p class="form-control-static">{{$clean_landmark->nose_left_alar_out_tip_y}}</p>
                </div>
                <div class="form-group">
                    <label for="nose_right_alar_out_tip_x">NOSE_RIGHT_ALAR_OUT_TIP_X</label>

                    <p class="form-control-static">{{$clean_landmark->nose_right_alar_out_tip_x}}</p>
                </div>
                <div class="form-group">
                    <label for="nose_right_alar_out_tip_y">NOSE_RIGHT_ALAR_OUT_TIP_Y</label>

                    <p class="form-control-static">{{$clean_landmark->nose_right_alar_out_tip_y}}</p>
                </div>
                <div class="form-group">
                    <label for="upper_lip_top_x">UPPER_LIP_TOP_X</label>

                    <p class="form-control-static">{{$clean_landmark->upper_lip_top_x}}</p>
                </div>
                <div class="form-group">
                    <label for="upper_lip_top_y">UPPER_LIP_TOP_Y</label>

                    <p class="form-control-static">{{$clean_landmark->upper_lip_top_y}}</p>
                </div>
                <div class="form-group">
                    <label for="upper_lip_bottom_x">UPPER_LIP_BOTTOM_X</label>

                    <p class="form-control-static">{{$clean_landmark->upper_lip_bottom_x}}</p>
                </div>
                <div class="form-group">
                    <label for="upper_lip_bottom_y">UPPER_LIP_BOTTOM_Y</label>

                    <p class="form-control-static">{{$clean_landmark->upper_lip_bottom_y}}</p>
                </div>
                <div class="form-group">
                    <label for="under_lip_top_x">UNDER_LIP_TOP_X</label>

                    <p class="form-control-static">{{$clean_landmark->under_lip_top_x}}</p>
                </div>
                <div class="form-group">
                    <label for="under_lip_top_y">UNDER_LIP_TOP_Y</label>

                    <p class="form-control-static">{{$clean_landmark->under_lip_top_y}}</p>
                </div>
                <div class="form-group">
                    <label for="under_lip_bottom_x">UNDER_LIP_BOTTOM_X</label>

                    <p class="form-control-static">{{$clean_landmark->under_lip_bottom_x}}</p>
                </div>
                <div class="form-group">
                    <label for="under_lip_bottom_y">UNDER_LIP_BOTTOM_Y</label>

                    <p class="form-control-static">{{$clean_landmark->under_lip_bottom_y}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('clean_landmarks.index') }}"><i
                        class="glyphicon glyphicon-backward"></i> Back</a>

        </div>
    </div>

@endsection