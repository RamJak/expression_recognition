{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('pupil_left_x', 'Pupil_left_x:') !!}
			{!! Form::text('pupil_left_x') !!}
		</li>
		<li>
			{!! Form::label('pupil_left_y', 'Pupil_left_y:') !!}
			{!! Form::text('pupil_left_y') !!}
		</li>
		<li>
			{!! Form::label('pupil_right_x', 'Pupil_right_x:') !!}
			{!! Form::text('pupil_right_x') !!}
		</li>
		<li>
			{!! Form::label('pupil_right_y', 'Pupil_right_y:') !!}
			{!! Form::text('pupil_right_y') !!}
		</li>
		<li>
			{!! Form::label('nose_tip_x', 'Nose_tip_x:') !!}
			{!! Form::text('nose_tip_x') !!}
		</li>
		<li>
			{!! Form::label('nose_tip_y', 'Nose_tip_y:') !!}
			{!! Form::text('nose_tip_y') !!}
		</li>
		<li>
			{!! Form::label('mouth_left_x', 'Mouth_left_x:') !!}
			{!! Form::text('mouth_left_x') !!}
		</li>
		<li>
			{!! Form::label('mouth_left_y', 'Mouth_left_y:') !!}
			{!! Form::text('mouth_left_y') !!}
		</li>
		<li>
			{!! Form::label('mouth_right_x', 'Mouth_right_x:') !!}
			{!! Form::text('mouth_right_x') !!}
		</li>
		<li>
			{!! Form::label('mouth_right_y', 'Mouth_right_y:') !!}
			{!! Form::text('mouth_right_y') !!}
		</li>
		<li>
			{!! Form::label('eyebrow_left_outer_x', 'Eyebrow_left_outer_x:') !!}
			{!! Form::text('eyebrow_left_outer_x') !!}
		</li>
		<li>
			{!! Form::label('eyebrow_left_outer_y', 'Eyebrow_left_outer_y:') !!}
			{!! Form::text('eyebrow_left_outer_y') !!}
		</li>
		<li>
			{!! Form::label('eyebrow_left_inner_x', 'Eyebrow_left_inner_x:') !!}
			{!! Form::text('eyebrow_left_inner_x') !!}
		</li>
		<li>
			{!! Form::label('eyebrow_left_inner_y', 'Eyebrow_left_inner_y:') !!}
			{!! Form::text('eyebrow_left_inner_y') !!}
		</li>
		<li>
			{!! Form::label('eye_left_outer_x', 'Eye_left_outer_x:') !!}
			{!! Form::text('eye_left_outer_x') !!}
		</li>
		<li>
			{!! Form::label('eye_left_outer_y', 'Eye_left_outer_y:') !!}
			{!! Form::text('eye_left_outer_y') !!}
		</li>
		<li>
			{!! Form::label('eye_left_top_x', 'Eye_left_top_x:') !!}
			{!! Form::text('eye_left_top_x') !!}
		</li>
		<li>
			{!! Form::label('eye_left_top_y', 'Eye_left_top_y:') !!}
			{!! Form::text('eye_left_top_y') !!}
		</li>
		<li>
			{!! Form::label('eye_left_bottom_x', 'Eye_left_bottom_x:') !!}
			{!! Form::text('eye_left_bottom_x') !!}
		</li>
		<li>
			{!! Form::label('eye_left_bottom_y', 'Eye_left_bottom_y:') !!}
			{!! Form::text('eye_left_bottom_y') !!}
		</li>
		<li>
			{!! Form::label('eye_left_inner_x', 'Eye_left_inner_x:') !!}
			{!! Form::text('eye_left_inner_x') !!}
		</li>
		<li>
			{!! Form::label('eye_left_inner_y', 'Eye_left_inner_y:') !!}
			{!! Form::text('eye_left_inner_y') !!}
		</li>
		<li>
			{!! Form::label('eyebrow_right_inner_x', 'Eyebrow_right_inner_x:') !!}
			{!! Form::text('eyebrow_right_inner_x') !!}
		</li>
		<li>
			{!! Form::label('eyebrow_right_inner_y', 'Eyebrow_right_inner_y:') !!}
			{!! Form::text('eyebrow_right_inner_y') !!}
		</li>
		<li>
			{!! Form::label('eyebrow_right_outer_x', 'Eyebrow_right_outer_x:') !!}
			{!! Form::text('eyebrow_right_outer_x') !!}
		</li>
		<li>
			{!! Form::label('eyebrow_right_outer_y', 'Eyebrow_right_outer_y:') !!}
			{!! Form::text('eyebrow_right_outer_y') !!}
		</li>
		<li>
			{!! Form::label('eye_right_inner_x', 'Eye_right_inner_x:') !!}
			{!! Form::text('eye_right_inner_x') !!}
		</li>
		<li>
			{!! Form::label('eye_right_inner_y', 'Eye_right_inner_y:') !!}
			{!! Form::text('eye_right_inner_y') !!}
		</li>
		<li>
			{!! Form::label('eye_right_top_x', 'Eye_right_top_x:') !!}
			{!! Form::text('eye_right_top_x') !!}
		</li>
		<li>
			{!! Form::label('eye_right_top_y', 'Eye_right_top_y:') !!}
			{!! Form::text('eye_right_top_y') !!}
		</li>
		<li>
			{!! Form::label('eye_right_bottom_x', 'Eye_right_bottom_x:') !!}
			{!! Form::text('eye_right_bottom_x') !!}
		</li>
		<li>
			{!! Form::label('eye_right_bottom_y', 'Eye_right_bottom_y:') !!}
			{!! Form::text('eye_right_bottom_y') !!}
		</li>
		<li>
			{!! Form::label('eye_right_outer_x', 'Eye_right_outer_x:') !!}
			{!! Form::text('eye_right_outer_x') !!}
		</li>
		<li>
			{!! Form::label('eye_right_outer_y', 'Eye_right_outer_y:') !!}
			{!! Form::text('eye_right_outer_y') !!}
		</li>
		<li>
			{!! Form::label('nose_root_left_x', 'Nose_root_left_x:') !!}
			{!! Form::text('nose_root_left_x') !!}
		</li>
		<li>
			{!! Form::label('nose_root_left_y', 'Nose_root_left_y:') !!}
			{!! Form::text('nose_root_left_y') !!}
		</li>
		<li>
			{!! Form::label('nose_root_right_x', 'Nose_root_right_x:') !!}
			{!! Form::text('nose_root_right_x') !!}
		</li>
		<li>
			{!! Form::label('nose_root_right_y', 'Nose_root_right_y:') !!}
			{!! Form::text('nose_root_right_y') !!}
		</li>
		<li>
			{!! Form::label('nose_left_alar_top_x', 'Nose_left_alar_top_x:') !!}
			{!! Form::text('nose_left_alar_top_x') !!}
		</li>
		<li>
			{!! Form::label('nose_left_alar_top_y', 'Nose_left_alar_top_y:') !!}
			{!! Form::text('nose_left_alar_top_y') !!}
		</li>
		<li>
			{!! Form::label('nose_right_alar_top_x', 'Nose_right_alar_top_x:') !!}
			{!! Form::text('nose_right_alar_top_x') !!}
		</li>
		<li>
			{!! Form::label('nose_right_alar_top_y', 'Nose_right_alar_top_y:') !!}
			{!! Form::text('nose_right_alar_top_y') !!}
		</li>
		<li>
			{!! Form::label('nose_left_alar_out_tip_x', 'Nose_left_alar_out_tip_x:') !!}
			{!! Form::text('nose_left_alar_out_tip_x') !!}
		</li>
		<li>
			{!! Form::label('nose_left_alar_out_tip_y', 'Nose_left_alar_out_tip_y:') !!}
			{!! Form::text('nose_left_alar_out_tip_y') !!}
		</li>
		<li>
			{!! Form::label('nose_right_alar_out_tip_x', 'Nose_right_alar_out_tip_x:') !!}
			{!! Form::text('nose_right_alar_out_tip_x') !!}
		</li>
		<li>
			{!! Form::label('nose_right_alar_out_tip_y', 'Nose_right_alar_out_tip_y:') !!}
			{!! Form::text('nose_right_alar_out_tip_y') !!}
		</li>
		<li>
			{!! Form::label('upper_lip_top', 'Upper_lip_top:') !!}
			{!! Form::text('upper_lip_top') !!}
		</li>
		<li>
			{!! Form::label('upper_lip_top_y', 'Upper_lip_top_y:') !!}
			{!! Form::text('upper_lip_top_y') !!}
		</li>
		<li>
			{!! Form::label('upper_lip_bottom_x', 'Upper_lip_bottom_x:') !!}
			{!! Form::text('upper_lip_bottom_x') !!}
		</li>
		<li>
			{!! Form::label('upper_lip_bottom_y', 'Upper_lip_bottom_y:') !!}
			{!! Form::text('upper_lip_bottom_y') !!}
		</li>
		<li>
			{!! Form::label('under_lip_top_x', 'Under_lip_top_x:') !!}
			{!! Form::text('under_lip_top_x') !!}
		</li>
		<li>
			{!! Form::label('under_lip_top_y', 'Under_lip_top_y:') !!}
			{!! Form::text('under_lip_top_y') !!}
		</li>
		<li>
			{!! Form::label('under_lip_bottom_x', 'Under_lip_bottom_x:') !!}
			{!! Form::text('under_lip_bottom_x') !!}
		</li>
		<li>
			{!! Form::label('under_lip_bottom_y', 'Under_lip_bottom_y:') !!}
			{!! Form::text('under_lip_bottom_y') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}