<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('faces', function(Blueprint $table) {
			$table->foreign('landmark_id')->references('id')->on('landmarks')
						->onDelete('no action')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::table('faces', function(Blueprint $table) {
			$table->dropForeign('faces_landmark_id_foreign');
		});
	}
}