<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use ExpressionRecognition\Face;

class CreateFacesTable extends Migration {

	public function up()
	{
		Schema::create('faces', function(Blueprint $table) {
			$table->increments('id');
			$table->string('face_id');
			$table->string('url');
			$table->decimal('width');
			$table->decimal('height');
			$table->decimal('top');
			$table->decimal('left');
			$table->string('gender', 20);
			$table->integer('age');
			$table->decimal('pose_pitch');
			$table->decimal('pose_roll');
			$table->decimal('pose_yaw');
			$table->decimal('fh_moustache');
			$table->decimal('fh_beard');
			$table->decimal('fh_sideburns');
            $table->string('status', 50);
            $table->enum('expression', array_merge([""], Face::$expression));
			$table->integer('landmark_id')->unsigned();
            $table->integer('query_id')->unsigned();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('faces');
	}
}
