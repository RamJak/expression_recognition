<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandmarksTable extends Migration {

	public function up()
	{
		Schema::create('landmarks', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->decimal('pupil_left_x');
			$table->decimal('pupil_left_y');
			$table->decimal('pupil_right_x');
			$table->decimal('pupil_right_y');
			$table->decimal('nose_tip_x');
			$table->decimal('nose_tip_y');
			$table->decimal('mouth_left_x');
			$table->decimal('mouth_left_y');
			$table->decimal('mouth_right_x');
			$table->decimal('mouth_right_y');
			$table->decimal('eyebrow_left_outer_x');
			$table->decimal('eyebrow_left_outer_y');
			$table->decimal('eyebrow_left_inner_x');
			$table->decimal('eyebrow_left_inner_y');
			$table->decimal('eye_left_outer_x');
			$table->decimal('eye_left_outer_y');
			$table->decimal('eye_left_top_x');
			$table->decimal('eye_left_top_y');
			$table->decimal('eye_left_bottom_x');
			$table->decimal('eye_left_bottom_y');
			$table->decimal('eye_left_inner_x');
			$table->decimal('eye_left_inner_y');
			$table->decimal('eyebrow_right_inner_x');
			$table->decimal('eyebrow_right_inner_y');
			$table->decimal('eyebrow_right_outer_x');
			$table->decimal('eyebrow_right_outer_y');
			$table->decimal('eye_right_inner_x');
			$table->decimal('eye_right_inner_y');
			$table->decimal('eye_right_top_x');
			$table->decimal('eye_right_top_y');
			$table->decimal('eye_right_bottom_x');
			$table->decimal('eye_right_bottom_y');
			$table->decimal('eye_right_outer_x');
			$table->decimal('eye_right_outer_y');
			$table->decimal('nose_root_left_x');
			$table->decimal('nose_root_left_y');
			$table->decimal('nose_root_right_x');
			$table->decimal('nose_root_right_y');
			$table->decimal('nose_left_alar_top_x');
			$table->decimal('nose_left_alar_top_y');
			$table->decimal('nose_right_alar_top_x');
			$table->decimal('nose_right_alar_top_y');
			$table->decimal('nose_left_alar_out_tip_x');
			$table->decimal('nose_left_alar_out_tip_y');
			$table->decimal('nose_right_alar_out_tip_x');
			$table->decimal('nose_right_alar_out_tip_y');
			$table->decimal('upper_lip_top_x');
			$table->decimal('upper_lip_top_y');
			$table->decimal('upper_lip_bottom_x');
			$table->decimal('upper_lip_bottom_y');
			$table->decimal('under_lip_top_x');
			$table->decimal('under_lip_top_y');
			$table->decimal('under_lip_bottom_x');
			$table->decimal('under_lip_bottom_y');
		});
	}

	public function down()
	{
		Schema::drop('landmarks');
	}
}
