<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCleanLandmarksTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clean_landmarks', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('pupil_left_x');
            $table->decimal('pupil_left_y');
            $table->decimal('pupil_right_x');
            $table->decimal('pupil_right_y');
            $table->decimal('nose_tip_x');
            $table->decimal('nose_tip_y');
            $table->decimal('mouth_left_x');
            $table->decimal('mouth_left_y');
            $table->decimal('mouth_right_x');
            $table->decimal('mouth_right_y');
            $table->decimal('eyebrow_left_outer_x');
            $table->decimal('eyebrow_left_outer_y');
            $table->decimal('eyebrow_left_inner_x');
            $table->decimal('eyebrow_left_inner_y');
            $table->decimal('eye_left_outer_x');
            $table->decimal('eye_left_outer_y');
            $table->decimal('eye_left_top_x');
            $table->decimal('eye_left_top_y');
            $table->decimal('eye_left_bottom_x');
            $table->decimal('eye_left_bottom_y');
            $table->decimal('eye_left_inner_x');
            $table->decimal('eye_left_inner_y');
            $table->decimal('eyebrow_right_inner_x');
            $table->decimal('eyebrow_right_inner_y');
            $table->decimal('eyebrow_right_outer_x');
            $table->decimal('eyebrow_right_outer_y');
            $table->decimal('eye_right_inner_x');
            $table->decimal('eye_right_inner_y');
            $table->decimal('eye_right_top_x');
            $table->decimal('eye_right_top_y');
            $table->decimal('eye_right_bottom_x');
            $table->decimal('eye_right_bottom_y');
            $table->decimal('eye_right_outer_x');
            $table->decimal('eye_right_outer_y');
            $table->decimal('nose_root_left_x');
            $table->decimal('nose_root_left_y');
            $table->decimal('nose_root_right_x');
            $table->decimal('nose_root_right_y');
            $table->decimal('nose_left_alar_top_x');
            $table->decimal('nose_left_alar_top_y');
            $table->decimal('nose_right_alar_top_x');
            $table->decimal('nose_right_alar_top_y');
            $table->decimal('nose_left_alar_out_tip_x');
            $table->decimal('nose_left_alar_out_tip_y');
            $table->decimal('nose_right_alar_out_tip_x');
            $table->decimal('nose_right_alar_out_tip_y');
            $table->decimal('upper_lip_top_x');
            $table->decimal('upper_lip_top_y');
            $table->decimal('upper_lip_bottom_x');
            $table->decimal('upper_lip_bottom_y');
            $table->decimal('under_lip_top_x');
            $table->decimal('under_lip_top_y');
            $table->decimal('under_lip_bottom_x');
            $table->decimal('under_lip_bottom_y');
            $table->timestamps();
        });

        Schema::table('faces', function (Blueprint $table) {
            $table->integer('clean_landmark_id')->unsigned()->after("landmark_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clean_landmarks');

        Schema::table('faces', function (Blueprint $table) {
            $table->dropColumn('clean_landmark_id');
        });
    }

}
