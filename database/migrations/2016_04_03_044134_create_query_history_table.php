<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use ExpressionRecognition\Face;

class CreateQueryHistoryTable extends Migration {

	public function up()
	{
        Schema::create('query', function (Blueprint $table) {
			$table->increments('id');
			$table->enum('expression', Face::$expression);
			$table->string('query', 100);
			$table->integer('top');
            $table->integer('skip');
			$table->timestamps();
		});
	}

	public function down()
	{
        Schema::drop('query');
	}
}
