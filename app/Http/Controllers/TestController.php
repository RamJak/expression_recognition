<?php

namespace ExpressionRecognition\Http\Controllers;

use ExpressionRecognition\RetrieveExpressions as Retriever;
use ExpressionRecognition\Face;
use ExpressionRecognition\Landmark;
use Illuminate\Support\Facades\Artisan;

class TestController extends Controller
{
    //
    protected $retriever;

    public function retrieve(Retriever $retriever)
    {
      Artisan::call('expression:retrieve', [
          'expression-type' => "laugh",
      ]);
    }
}
