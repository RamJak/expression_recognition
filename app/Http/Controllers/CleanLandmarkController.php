<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use ExpressionRecognition\CleanLandmark;
use Illuminate\Http\Request;

class CleanLandmarkController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $clean_landmarks = CleanLandmark::orderBy('id', 'desc')->paginate(10);

        return view('clean_landmarks.index', compact('clean_landmarks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('clean_landmarks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $clean_landmark = new CleanLandmark();

        $clean_landmark->pupil_left_x = $request->input("pupil_left_x");
        $clean_landmark->pupil_left_y = $request->input("pupil_left_y");
        $clean_landmark->pupil_right_x = $request->input("pupil_right_x");
        $clean_landmark->pupil_right_y = $request->input("pupil_right_y");
        $clean_landmark->nose_tip_x = $request->input("nose_tip_x");
        $clean_landmark->nose_tip_y = $request->input("nose_tip_y");
        $clean_landmark->mouth_left_x = $request->input("mouth_left_x");
        $clean_landmark->mouth_left_y = $request->input("mouth_left_y");
        $clean_landmark->mouth_right_x = $request->input("mouth_right_x");
        $clean_landmark->mouth_right_y = $request->input("mouth_right_y");
        $clean_landmark->eyebrow_left_outer_x = $request->input("eyebrow_left_outer_x");
        $clean_landmark->eyebrow_left_outer_y = $request->input("eyebrow_left_outer_y");
        $clean_landmark->eyebrow_left_inner_x = $request->input("eyebrow_left_inner_x");
        $clean_landmark->eyebrow_left_inner_y = $request->input("eyebrow_left_inner_y");
        $clean_landmark->eye_left_outer_x = $request->input("eye_left_outer_x");
        $clean_landmark->eye_left_outer_y = $request->input("eye_left_outer_y");
        $clean_landmark->eye_left_top_x = $request->input("eye_left_top_x");
        $clean_landmark->eye_left_top_y = $request->input("eye_left_top_y");
        $clean_landmark->eye_left_bottom_x = $request->input("eye_left_bottom_x");
        $clean_landmark->eye_left_bottom_y = $request->input("eye_left_bottom_y");
        $clean_landmark->eye_left_inner_x = $request->input("eye_left_inner_x");
        $clean_landmark->eye_left_inner_y = $request->input("eye_left_inner_y");
        $clean_landmark->eyebrow_right_inner_x = $request->input("eyebrow_right_inner_x");
        $clean_landmark->eyebrow_right_inner_y = $request->input("eyebrow_right_inner_y");
        $clean_landmark->eyebrow_right_outer_x = $request->input("eyebrow_right_outer_x");
        $clean_landmark->eyebrow_right_outer_y = $request->input("eyebrow_right_outer_y");
        $clean_landmark->eye_right_inner_x = $request->input("eye_right_inner_x");
        $clean_landmark->eye_right_inner_y = $request->input("eye_right_inner_y");
        $clean_landmark->eye_right_top_x = $request->input("eye_right_top_x");
        $clean_landmark->eye_right_top_y = $request->input("eye_right_top_y");
        $clean_landmark->eye_right_bottom_x = $request->input("eye_right_bottom_x");
        $clean_landmark->eye_right_bottom_y = $request->input("eye_right_bottom_y");
        $clean_landmark->eye_right_outer_x = $request->input("eye_right_outer_x");
        $clean_landmark->eye_right_outer_y = $request->input("eye_right_outer_y");
        $clean_landmark->nose_root_left_x = $request->input("nose_root_left_x");
        $clean_landmark->nose_root_left_y = $request->input("nose_root_left_y");
        $clean_landmark->nose_root_right_x = $request->input("nose_root_right_x");
        $clean_landmark->nose_root_right_y = $request->input("nose_root_right_y");
        $clean_landmark->nose_left_alar_top_x = $request->input("nose_left_alar_top_x");
        $clean_landmark->nose_left_alar_top_y = $request->input("nose_left_alar_top_y");
        $clean_landmark->nose_right_alar_top_x = $request->input("nose_right_alar_top_x");
        $clean_landmark->nose_right_alar_top_y = $request->input("nose_right_alar_top_y");
        $clean_landmark->nose_left_alar_out_tip_x = $request->input("nose_left_alar_out_tip_x");
        $clean_landmark->nose_left_alar_out_tip_y = $request->input("nose_left_alar_out_tip_y");
        $clean_landmark->nose_right_alar_out_tip_x = $request->input("nose_right_alar_out_tip_x");
        $clean_landmark->nose_right_alar_out_tip_y = $request->input("nose_right_alar_out_tip_y");
        $clean_landmark->upper_lip_top_x = $request->input("upper_lip_top_x");
        $clean_landmark->upper_lip_top_y = $request->input("upper_lip_top_y");
        $clean_landmark->upper_lip_bottom_x = $request->input("upper_lip_bottom_x");
        $clean_landmark->upper_lip_bottom_y = $request->input("upper_lip_bottom_y");
        $clean_landmark->under_lip_top_x = $request->input("under_lip_top_x");
        $clean_landmark->under_lip_top_y = $request->input("under_lip_top_y");
        $clean_landmark->under_lip_bottom_x = $request->input("under_lip_bottom_x");
        $clean_landmark->under_lip_bottom_y = $request->input("under_lip_bottom_y");

        $clean_landmark->save();

        return redirect()->route('clean_landmarks.index')->with('message', 'Item created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $clean_landmark = CleanLandmark::findOrFail($id);

        return view('clean_landmarks.show', compact('clean_landmark'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $clean_landmark = CleanLandmark::findOrFail($id);

        return view('clean_landmarks.edit', compact('clean_landmark'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $clean_landmark = CleanLandmark::findOrFail($id);

        $clean_landmark->pupil_left_x = $request->input("pupil_left_x");
        $clean_landmark->pupil_left_y = $request->input("pupil_left_y");
        $clean_landmark->pupil_right_x = $request->input("pupil_right_x");
        $clean_landmark->pupil_right_y = $request->input("pupil_right_y");
        $clean_landmark->nose_tip_x = $request->input("nose_tip_x");
        $clean_landmark->nose_tip_y = $request->input("nose_tip_y");
        $clean_landmark->mouth_left_x = $request->input("mouth_left_x");
        $clean_landmark->mouth_left_y = $request->input("mouth_left_y");
        $clean_landmark->mouth_right_x = $request->input("mouth_right_x");
        $clean_landmark->mouth_right_y = $request->input("mouth_right_y");
        $clean_landmark->eyebrow_left_outer_x = $request->input("eyebrow_left_outer_x");
        $clean_landmark->eyebrow_left_outer_y = $request->input("eyebrow_left_outer_y");
        $clean_landmark->eyebrow_left_inner_x = $request->input("eyebrow_left_inner_x");
        $clean_landmark->eyebrow_left_inner_y = $request->input("eyebrow_left_inner_y");
        $clean_landmark->eye_left_outer_x = $request->input("eye_left_outer_x");
        $clean_landmark->eye_left_outer_y = $request->input("eye_left_outer_y");
        $clean_landmark->eye_left_top_x = $request->input("eye_left_top_x");
        $clean_landmark->eye_left_top_y = $request->input("eye_left_top_y");
        $clean_landmark->eye_left_bottom_x = $request->input("eye_left_bottom_x");
        $clean_landmark->eye_left_bottom_y = $request->input("eye_left_bottom_y");
        $clean_landmark->eye_left_inner_x = $request->input("eye_left_inner_x");
        $clean_landmark->eye_left_inner_y = $request->input("eye_left_inner_y");
        $clean_landmark->eyebrow_right_inner_x = $request->input("eyebrow_right_inner_x");
        $clean_landmark->eyebrow_right_inner_y = $request->input("eyebrow_right_inner_y");
        $clean_landmark->eyebrow_right_outer_x = $request->input("eyebrow_right_outer_x");
        $clean_landmark->eyebrow_right_outer_y = $request->input("eyebrow_right_outer_y");
        $clean_landmark->eye_right_inner_x = $request->input("eye_right_inner_x");
        $clean_landmark->eye_right_inner_y = $request->input("eye_right_inner_y");
        $clean_landmark->eye_right_top_x = $request->input("eye_right_top_x");
        $clean_landmark->eye_right_top_y = $request->input("eye_right_top_y");
        $clean_landmark->eye_right_bottom_x = $request->input("eye_right_bottom_x");
        $clean_landmark->eye_right_bottom_y = $request->input("eye_right_bottom_y");
        $clean_landmark->eye_right_outer_x = $request->input("eye_right_outer_x");
        $clean_landmark->eye_right_outer_y = $request->input("eye_right_outer_y");
        $clean_landmark->nose_root_left_x = $request->input("nose_root_left_x");
        $clean_landmark->nose_root_left_y = $request->input("nose_root_left_y");
        $clean_landmark->nose_root_right_x = $request->input("nose_root_right_x");
        $clean_landmark->nose_root_right_y = $request->input("nose_root_right_y");
        $clean_landmark->nose_left_alar_top_x = $request->input("nose_left_alar_top_x");
        $clean_landmark->nose_left_alar_top_y = $request->input("nose_left_alar_top_y");
        $clean_landmark->nose_right_alar_top_x = $request->input("nose_right_alar_top_x");
        $clean_landmark->nose_right_alar_top_y = $request->input("nose_right_alar_top_y");
        $clean_landmark->nose_left_alar_out_tip_x = $request->input("nose_left_alar_out_tip_x");
        $clean_landmark->nose_left_alar_out_tip_y = $request->input("nose_left_alar_out_tip_y");
        $clean_landmark->nose_right_alar_out_tip_x = $request->input("nose_right_alar_out_tip_x");
        $clean_landmark->nose_right_alar_out_tip_y = $request->input("nose_right_alar_out_tip_y");
        $clean_landmark->upper_lip_top_x = $request->input("upper_lip_top_x");
        $clean_landmark->upper_lip_top_y = $request->input("upper_lip_top_y");
        $clean_landmark->upper_lip_bottom_x = $request->input("upper_lip_bottom_x");
        $clean_landmark->upper_lip_bottom_y = $request->input("upper_lip_bottom_y");
        $clean_landmark->under_lip_top_x = $request->input("under_lip_top_x");
        $clean_landmark->under_lip_top_y = $request->input("under_lip_top_y");
        $clean_landmark->under_lip_bottom_x = $request->input("under_lip_bottom_x");
        $clean_landmark->under_lip_bottom_y = $request->input("under_lip_bottom_y");

        $clean_landmark->save();

        return redirect()->route('clean_landmarks.index')->with('message', 'Item updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $clean_landmark = CleanLandmark::findOrFail($id);
        $clean_landmark->delete();

        return redirect()->route('clean_landmarks.index')->with('message', 'Item deleted successfully.');
    }

}
