<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


use ExpressionRecognition\RetrieveExpressions;
use GuzzleHttp\Client;

//Route::get('/', 'TestController@retrieve');
Route::get('/temp', function () {
    $client = new Client();
    $retriever = new RetrieveExpressions($client);
    $landmarks = $retriever->retrieveLandmarks('www.google.com');
    dd($landmarks);
});

Route::resource('queryhistory', 'QueryHistoryController');
Route::resource('face', 'FaceController');
Route::resource('landmark', 'LandmarkController');