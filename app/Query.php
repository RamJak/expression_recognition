<?php

namespace ExpressionRecognition;

use Illuminate\Database\Eloquent\Model;

class Query extends Model
{
    public static $defaultTop = 50;
    public static $default_query = [
        "neutral expression",
        "smile",
        "laugh",
        "sad",
        "angry",
        "disgusted",
        "scared"
    ];
	public $timestamps = true;
    protected $table = 'query';
	protected $guarded = [];

    public function faces()
    {
        return $this->hasMany('Face');
    }
}
