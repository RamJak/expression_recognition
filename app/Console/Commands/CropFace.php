<?php

namespace ExpressionRecognition\Console\Commands;

use ExpressionRecognition\Face;
use ExpressionRecognition\RetrieveExpressions as Engine;
use Illuminate\Console\Command;

class CropFace extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expression:normalize';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crop face data from database';

    protected $engine;

    /**
     * Create a new command instance.
     *
     * @param RetrieveExpressions $engine
     */
    public function __construct(Engine $engine)
    {
        parent::__construct();

        $this->engine = $engine;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $faces = Face::where('status', 'like', 'OK%')->get();
        foreach ($faces as $face) {
            $this->engine->cropFace([$face->id]);
            $this->line('Normalized face no. ' . $face->id);
        }
        $this->info('Normalized all OK data');
    }
}
