<?php

namespace ExpressionRecognition\Console\Commands;

use Illuminate\Console\Command;
use ExpressionRecognition\RetrieveExpressions as Retriever;
use ExpressionRecognition\Face;
use ExpressionRecognition\Query;
use Illuminate\Support\Facades\DB;

class RetrieveExpressions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expression:retrieve {expression-type : pre-defined (neutral,smile,laugh,sadness,anger,disgust,fear)} {--Q|query= : Bing search query} {--T|target= : Minimum data acquired}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve expression picture from Bing, processing with Face API and finally insert to database';

    protected $retriever;

    /**
     * Create a new command instance.
     *
     * @param Retriever $retriever
     */
    public function __construct(Retriever $retriever)
    {
        parent::__construct();

        $this->retriever = $retriever;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $target = 200;
        if ($this->option('target') && is_numeric($this->option('target'))) {
            $target = $this->option('target');
        } elseif (!is_numeric($this->option('target'))) {
            $this->error('target must be a number');
            return;
        }
        //cek pre-defined
        if (!in_array($expression_type = $this->argument('expression-type'), Face::$expression)) {
            $this->error('expression-type is pre-defined (' . implode(',', Face::$expression) . ')');
            return;
        }
        $query = (!is_null($this->option('query'))) ? $this->option('query') : Query::$default_query[array_keys(Face::$expression,
            $expression_type)[0]];
        //        var_dump($query); die();
        $this->info('Sending Query Request');
        //todo: temporary fix
        $skip = null;
        $top = null;
        if ($top == null) {
            $top = Query::$defaultTop;
        }
        $count = 0;
        $query_count = DB::table('training')->where('expression', $expression_type)->count();

        while ($target > $query_count) {
            if ($skip == null) {
                $log = Query::where('query', $query)->orderBy('skip', 'desc')->first();
                $skip = ($log == null) ? 0 : $log->skip + $top;
            }
            $queryResult = $this->retriever->retrieve($query, $skip + 1);
            $query_id = $this->retriever->saveLog($this->argument('expression-type'), $query, $skip);

            $this->info('Filtering Result');
            $result = $this->retriever->imageFilter($queryResult);
            $queryResult = $result->filteredResult;
            $this->info('Total Result = ' . ($result->positiveCount + $result->undersizeCount + $result->unmatchTypeCount) . ', Positive Result = ' . $result->positiveCount . ', Negative Result = ' . ($result->undersizeCount + $result->unmatchTypeCount) . ' (Undersize = ' . $result->undersizeCount . '|Unmatch Type = ' . $result->unmatchTypeCount . ')');
            $this->line('');

            $this->info('Sending Face API Requests');
            $total_result = count($queryResult->results);
            // $bar = $this->output->createProgressBar($total_result);
            $positive_result = 0;
            $negative_result = 0;

            $time = time();

            $result_id = [];
            //todo: ganti status jadi angka?
            foreach ($queryResult->results as $key2) {
                if ($count++ == 20) {
                    $this->info('wait for limit');
                    sleep($time + 61 - time());
                    $count = 1;
                    $time = time();
                }
                $landmarks = $this->retriever->retrieveLandmarks($key2->MediaUrl);
                foreach ($landmarks->status as $key) {
                    $message = ($key == "OK\t\t\t") ? "(+)" : "(-)";
                    if ($key == "OK\t\t\t")
                        $positive_result++;
                    else
                        $negative_result++;
                    $this->line($message . " $key\t$key2->MediaUrl");
                    $temp = $this->retriever->insertToDatabase($landmarks->response, $key2->MediaUrl, $key, $query_id);
                    $result_id = array_merge($temp, $result_id);
                }
                // $bar->advance();
            }
            $this->retriever->cropFace($result_id);

            // $bar->finish();
            $this->info('Total Result = ' . $total_result . ', Positive Result = ' . $positive_result . ' , Negative Result = ' . $negative_result);
            //select * from clean_landmarks, faces, query where faces.status like 'OK%' and clean_landmarks.id = faces.clean_landmark_id and query.id = faces.query_id
            $query_count = DB::table('training')->where('expression', $expression_type)->count();
        }
    }
}