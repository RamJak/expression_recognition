<?php

namespace ExpressionRecognition;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Landmark extends Model {
	public $timestamps = true;
    protected $table = 'landmarks';
	protected $guarded = [];

	use SoftDeletes;

	protected $dates = ['deleted_at'];

	public function face()
	{
		$this->belongsTo('Face');
	}
}
