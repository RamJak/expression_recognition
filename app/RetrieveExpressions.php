<?php

namespace ExpressionRecognition;

/**
 * Retrieve Image from Expression Query
 */

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class RetrieveExpressions
{

    public static $acceptedType = ['jpeg', 'bmp', 'png'];
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
        // $this->client->setDefaultOption('verify', [false]);
    }

    public function retrieve($query, $skip = null, $top = null)
    {
        $query = urlencode($query);
        $response = $this->client->request(
            'GET',
            'https://api.datamarket.azure.com/Bing/Search/v1/Image?$top=' . $top . '&$skip=' . $skip . '&$format=json&Query=%27' . $query . '%27&Options=%27DisableLocationDetection%27&Adult=%27Off%27&ImageFilters=%27Face%3AFace%27',
            [
                'auth' => ['6kDhKIpci5wWiwiK/xNB2m5gptDInDcBTYkRWASfEUk', '6kDhKIpci5wWiwiK/xNB2m5gptDInDcBTYkRWASfEUk'],
            ]
        );

        return json_decode($response->getBody()->getContents())->d;
    }

    /***
     * @param $rawResult
     * @return \stdClass Filter Result
     */
    public function imageFilter($rawResult)
    {
        $result = new \stdClass();
        $result->positiveCount = 0;
        $result->undersizeCount = 0;
        $result->unmatchTypeCount = 0;
        $result->filteredResult = $rawResult;
        // dd(self::$acceptedType);
        foreach ($result->filteredResult->results as $key => $value) {
            if ($value->Width <= Face::$minSize || $value->Height <= Face::$minSize) {
                unset($result->filteredResult->results[$key]);
                $result->undersizeCount++;
            } else if (!in_array(explode('/', $value->ContentType)[1], self::$acceptedType)) {
                unset($result->filteredResult->results[$key]);
                $result->unmatchTypeCount++;
            } else
                $result->positiveCount++;
        }
        return $result;
    }

    public function retrieveLandmarks($url)
    {
        $result = new \stdClass();
        try {
            $response = $this->client->request(
                'POST',
                'https://api.projectoxford.ai/face/v1.0/detect?returnFaceLandmarks=true&returnFaceAttributes=age,gender,headPose,smile,facialHair,glasses',
                [
                    'json' => ['url' => $url],
                    'headers' => ['Ocp-Apim-Subscription-Key' => 'e5b613f1d5f84a2cb2aa1023e3f39d52']
                ]
            );
        } catch (RequestException $e) {
            $result->response = [];
            $result->status[] = "Request Error";
            \Log::error($e);
            return $result;
        }
        $result->response = json_decode($response->getBody()->getContents());
        if (empty($result->response))
            $result->status[] = "No Face Detected";
        else {
            foreach ($result->response as $key) {
                if ($key->faceRectangle->width <= Face::$minSize || $key->faceRectangle->height <= Face::$minSize) {
                    $result->status[] = "Face-part size is too small";
                } else {
                    $pose = $key->faceAttributes->headPose;
                    if (abs($pose->pitch) >= Face::$minRotation || abs($pose->roll) >= Face::$minRotation || abs($pose->yaw) >= Face::$minRotation) {
                        $result->status[] = "Head is too tilted\t";
                    } else {
                        $result->status[] = "OK\t\t\t";
                    }
                }
            }
        }
        return $result;
    }

    public function insertToDatabase($responses, $url, $status, $query_id)
    {
        $id = [];
        if (empty($response)) {
            $face = new Face;
            $face->query_id = $query_id;
            $face->status = $status;
            $face->url = $url;
        }
        foreach ($responses as $response) {
            $landmarks = new Landmark;
            foreach ($response->faceLandmarks as $key2 => $value) {
                $x = snake_case($key2) . '_x';
                $y = snake_case($key2) . '_y';
                $landmarks->$x = $value->x;
                $landmarks->$y = $value->y;
            }
            $landmarks->save();
            $face = new Face;
            $face->landmark_id = $landmarks->id;
            $face->query_id = $query_id;
            $face->status = $status;
            $face->url = $url;
            foreach ($response->faceRectangle as $key2 => $value) {
                $face->$key2 = $value;
            }
            $face->face_id = $response->faceId;
            $face->gender = $response->faceAttributes->gender;
            foreach ($response->faceAttributes->headPose as $key2 => $value) {
                $str = "pose_" . $key2;
                $face->$str = $value;
            }
            $face->age = (isset($response->faceAttributes->age)) ? $response->faceAttributes->age : 0;
            foreach ($response->faceAttributes->facialHair as $key2 => $value) {
                $str = "fh_" . $key2;
                $face->$str = $value;
            }
            $face->save();
            $id[] = $face->id;
        }
        return $id;
    }

    public function saveLog($expression, $query, $skip = 0, $top = null)
    {
        if ($top == null)
            $top = Query::$defaultTop;
        $log = new Query();
        $log->expression = $expression;
        $log->query = $query;
        $log->top = $top;
        $log->skip = $skip;
        $log->save();
        return $log->id;
    }

    //todo: normalize coordinate => ratio or change in prev func
    public function cropFace($face_ids = null)
    {
        $faces = ($face_ids == null) ? Face::where('status', 'like', 'OK%')->get() : Face::whereIn('id', $face_ids)->where('status', 'like', 'OK%')->get();
        //$landmarks = ($face_ids == null) ? Landmark::all() : Landmark::whereIn('id', Face::whereIn('id', [1, 2, 3, 4, 5])->pluck('landmark_id')->toArray())->get();
        $field = ['pupil_left_x', 'pupil_left_y', 'pupil_right_x', 'pupil_right_y', 'nose_tip_x', 'nose_tip_y', 'mouth_left_x', 'mouth_left_y', 'mouth_right_x', 'mouth_right_y', 'eyebrow_left_outer_x', 'eyebrow_left_outer_y', 'eyebrow_left_inner_x', 'eyebrow_left_inner_y', 'eye_left_outer_x', 'eye_left_outer_y', 'eye_left_top_x', 'eye_left_top_y', 'eye_left_bottom_x', 'eye_left_bottom_y', 'eye_left_inner_x', 'eye_left_inner_y', 'eyebrow_right_inner_x', 'eyebrow_right_inner_y', 'eyebrow_right_outer_x', 'eyebrow_right_outer_y', 'eye_right_inner_x', 'eye_right_inner_y', 'eye_right_top_x', 'eye_right_top_y', 'eye_right_bottom_x', 'eye_right_bottom_y', 'eye_right_outer_x', 'eye_right_outer_y', 'nose_root_left_x', 'nose_root_left_y', 'nose_root_right_x', 'nose_root_right_y', 'nose_left_alar_top_x', 'nose_left_alar_top_y', 'nose_right_alar_top_x', 'nose_right_alar_top_y', 'nose_left_alar_out_tip_x', 'nose_left_alar_out_tip_y', 'nose_right_alar_out_tip_x', 'nose_right_alar_out_tip_y', 'upper_lip_top_x', 'upper_lip_top_y', 'upper_lip_bottom_x', 'upper_lip_bottom_y', 'under_lip_top_x', 'under_lip_top_y', 'under_lip_bottom_x', 'under_lip_bottom_y'];

        foreach ($faces as $face) {
            $landmark = $face->landmark;
            //$ratiox = (left-val)/width*100
            //$ratioy = (top-val)/height*100
            $clean_landmark = (is_null($face->cleanLandmark)) ? new CleanLandmark : $face->cleanLandmark;
            //var_dump(is_null($face->cleanLandmark));
            $left = $face->left;
            $top = $face->top;
            $width = $face->width;
            $height = $face->height;
            foreach ($field as $item) {
                if (substr($item, -1) == 'x')
                    $clean_landmark->$item = ($landmark->$item - $left) / $width * 100;
                else
                    $clean_landmark->$item = ($landmark->$item - $top) / $height * 100;
            }
            $clean_landmark->save();
            $face->clean_landmark_id = $clean_landmark->id;
            $face->save();
        }
    }
}

?>
