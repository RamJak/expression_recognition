<?php

namespace ExpressionRecognition;

use Illuminate\Database\Eloquent\Model;

class CleanLandmark extends Model
{
    //
    public $timestamps = true;
    protected $guarded = [];
}
