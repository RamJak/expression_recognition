<?php

namespace ExpressionRecognition;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Face extends Model {

    public static $expression = [
        "neutral", "smile", "laugh", "sadness", "anger", "disgust", "fear"
    ];

    public static $minSize = 100;

	use SoftDeletes;
    public static $minRotation = 5;
    public $timestamps = true;

    //remember to make it an object if expressions get complicated
    protected $table = 'faces';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

	public function landmark()
	{
        return $this->belongsTo('ExpressionRecognition\Landmark');
	}

    public function from_query()
    {
        return $this->hasOne('Query');
    }

    public function cleanLandmark()
    {
        return $this->belongsTo('ExpressionRecognition\CleanLandmark');
    }
}
